// Copyright © 2023 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SG_SnakePawnBase.generated.h"

class UCameraComponent;
class ASG_GridActorBase;
class ASG_SnakeActorBase;

UCLASS()
class SNAKEGAME_API ASG_SnakePawnBase : public APawn
{
	GENERATED_BODY()

// Constructors and Deconstrucors block
public:

	ASG_SnakePawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* CameraComponent;


// Core game events block
protected:
	
	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;


// Inputs block
public:

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void OnMoveVertical(float InputAxisValue);

	void OnMoveHorizontal(float InputAxisValue);

// Dynamic camera block
public:

	void UpdatePawnLocationByGrid(const FIntPoint Dimensions, const int32 CellSize, const FTransform Transform);

	UPROPERTY(EditDefaultsOnly, meta = (Category = "Custom Camera Settings", ClampMin = "0", ClampMax = "1"))
	float CameraOffsetByZ{ 0.0f };

private:

	FIntPoint GridDimensions;
	uint32 GridCellSize;
	FTransform GridTransform;

	void OnViewportResized(FViewport* Viewport, uint32 Value);

	FDelegateHandle ViewportResizedHandle;


// Spawning Snake Actor block
public:

	void CreateSnakeActor();

	UPROPERTY(BlueprintReadWrite)
	ASG_SnakeActorBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASG_SnakeActorBase> SnakeActorClass;
};
