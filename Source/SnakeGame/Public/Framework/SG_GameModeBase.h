// Copyright © 2023 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SG_GameModeBase.generated.h"

class ASG_GridActorBase;

UCLASS()
class SNAKEGAME_API ASG_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

// Constructors and Deconstrucors block
public:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASG_GridActorBase> GridActorClass;

private:

	UPROPERTY()
	ASG_GridActorBase* GridActor;
};
