// Copyright © 2023 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/List.h"
#include "SG_SnakeActorBase.generated.h"

class ASG_SnakeElementBase;

UENUM()
enum class EMovementDirection
{
	Up,
	Down,
	Left,
	Right
};

UCLASS()
class SNAKEGAME_API ASG_SnakeActorBase : public AActor
{
	GENERATED_BODY()

// Constructors and Deconstrucors block
public:	

	ASG_SnakeActorBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ASG_SnakeElementBase> SnakeHeadClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ASG_SnakeElementBase> SnakeBodyClass;

	TDoubleLinkedList<ASG_SnakeElementBase*> SnakeElements;

	UPROPERTY()
	ASG_SnakeElementBase* SnakeHead;

	UPROPERTY()
	float ElementOffset;


// Core game events block
protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

// Creating Snake block;

public:

	void InitSnake(uint32 SnakeLength);

	void AddSnakeElement(uint32 ElementsNum);


// Movement block
public:

	void Move();

	UPROPERTY()
	EMovementDirection LastMovementDirection {EMovementDirection::Right};

	UPROPERTY(EditDefaultsOnly)
	float UpdateMoveTime {1.0f};

};
