// Copyright © 2023 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SG_GridActorBase.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ASG_GridActorBase : public AActor
{
	GENERATED_BODY()

// Constructors and Deconstrucors block
public:	

	ASG_GridActorBase();


// Core game events block
protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

// Initialization of grid
public:
	
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* FloorStaticMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "10", ClampMax = "100"), Category = "Grid Settings")
	FIntPoint Dimensions {10, 10};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "10", ClampMax = "100"), Category = "Grid Settings")
	int32 CellSize {10};

protected:

	UFUNCTION(BlueprintCallable)
	void UpdateFloorScaleBySettings();

	UFUNCTION(BlueprintCallable)
	void UpdateFloorMaterial();

private:

	UPROPERTY()
	UMaterialInstanceDynamic* GridMaterial;
};
