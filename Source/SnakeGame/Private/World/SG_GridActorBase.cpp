// Copyright © 2023 FaNtic, All rights reserved

#include "World/SG_GridActorBase.h"
#include "Components/StaticMeshComponent.h"

ASG_GridActorBase::ASG_GridActorBase()
{
	PrimaryActorTick.bCanEverTick = true;

	FloorStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	check(FloorStaticMeshComponent);
	FloorStaticMeshComponent->SetupAttachment(RootComponent);
}

void ASG_GridActorBase::BeginPlay()
{
	Super::BeginPlay();
}

void ASG_GridActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASG_GridActorBase::UpdateFloorScaleBySettings()
{
	int32 WorldWidth = Dimensions.Y * CellSize;
	int32 WorldHeight = Dimensions.X * CellSize;
	UStaticMesh* FloorStaticMesh = FloorStaticMeshComponent->GetStaticMesh();
	
	check(FloorStaticMesh);
	const FBox FloorBoundingBox = FloorStaticMesh->GetBoundingBox();
	const auto FloorSize = FloorBoundingBox.GetSize();

	check(FloorSize.X);
	check(FloorSize.Y);
	FloorStaticMeshComponent->SetRelativeScale3D(FVector(WorldHeight / FloorSize.X, WorldWidth / FloorSize.Y, 1.0f));
}

void ASG_GridActorBase::UpdateFloorMaterial()
{
	GridMaterial = FloorStaticMeshComponent->CreateAndSetMaterialInstanceDynamic(0);
	if (GridMaterial)
	{
		GridMaterial->SetVectorParameterValue("CellNum", FVector(Dimensions.X, Dimensions.Y, 0.0f));
	}
}
