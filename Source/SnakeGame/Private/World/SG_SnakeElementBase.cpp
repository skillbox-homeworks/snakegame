// Copyright © 2023 FaNtic, All rights reserved

#include "World/SG_SnakeElementBase.h"

ASG_SnakeElementBase::ASG_SnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = true;

	ElementStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Snake Element"));
	check(ElementStaticMeshComponent);
	ElementStaticMeshComponent->SetupAttachment(RootComponent);
}

void ASG_SnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

void ASG_SnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

