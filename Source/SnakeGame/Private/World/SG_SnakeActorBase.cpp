// Copyright © 2023 FaNtic, All rights reserved

#include "World/SG_SnakeActorBase.h"
#include "World/SG_SnakeElementBase.h"
#include "World/SG_GridActorBase.h"

ASG_SnakeActorBase::ASG_SnakeActorBase()
{
	PrimaryActorTick.bCanEverTick = true;

	ElementOffset = 100.0f;

	LastMovementDirection = EMovementDirection::Right;
	UpdateMoveTime = 1.0f;
}

void ASG_SnakeActorBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(UpdateMoveTime);

	InitSnake(4);
}

void ASG_SnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASG_SnakeActorBase::InitSnake(uint32 SnakeLength)
{
	check(GetWorld());

	// Adding snake head element
	SnakeHead = GetWorld()->SpawnActor<ASG_SnakeElementBase>(SnakeHeadClass, GetActorTransform());
	//SnakeHead->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	SnakeElements.AddHead(SnakeHead);

	// Adding snake body element [---<]
	for (uint32 i = 1; i < SnakeLength; ++i)
	{
		FVector NewSnakeElementLocation = SnakeHead->GetActorLocation() - FVector(0.0f, ElementOffset * i, 0.0f);
		ASG_SnakeElementBase* NewSnakeBodyElement = GetWorld()->SpawnActor<ASG_SnakeElementBase>(SnakeBodyClass, FTransform(NewSnakeElementLocation));
		//NewSnakeBodyElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		SnakeElements.AddTail(NewSnakeBodyElement);
	}
}

void ASG_SnakeActorBase::AddSnakeElement(uint32 ElementsNum)
{
	check(GetWorld());

	for (uint32 i = 0; i < ElementsNum; ++i)
	{
		FTransform NewSnakeElementTransform = SnakeElements.GetTail()->GetValue()->GetActorTransform();
		auto NewSnakeBodyElement = GetWorld()->SpawnActor<ASG_SnakeElementBase>(SnakeBodyClass, NewSnakeElementTransform);
		SnakeElements.AddTail(NewSnakeBodyElement);
	}
}

void ASG_SnakeActorBase::Move()
{
	FVector MovementVector = FVector::ZeroVector;
	float DeltaMoveDistance = ElementOffset;

	switch (LastMovementDirection)
	{
	case EMovementDirection::Up:
		MovementVector.X += DeltaMoveDistance;
		break;
	case EMovementDirection::Down:
		MovementVector.X -= DeltaMoveDistance;
		break;
	case EMovementDirection::Left:
		MovementVector.Y -= DeltaMoveDistance;
		break;
	case EMovementDirection::Right:
		MovementVector.Y += DeltaMoveDistance;
		break;
	}

	FVector HeadPreviousLocation = SnakeHead->GetActorLocation();
	SnakeHead->AddActorWorldOffset(MovementVector);

	ASG_SnakeElementBase* SnakeTail = SnakeElements.GetTail()->GetValue();
	SnakeTail->SetActorLocation(HeadPreviousLocation);
	SnakeElements.InsertNode(SnakeTail, SnakeElements.GetHead()->GetNextNode());
	SnakeElements.RemoveNode(SnakeElements.GetTail());
}

