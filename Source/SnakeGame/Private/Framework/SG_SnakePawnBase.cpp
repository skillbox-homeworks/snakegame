// Copyright © 2023 FaNtic, All rights reserved

#include "Framework/SG_SnakePawnBase.h"
#include "World/SG_SnakeActorBase.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

namespace
{
	float GetRadHalfHFOV(UCameraComponent* Camera)
	{
		return FMath::DegreesToRadians(Camera->FieldOfView * 0.5);
	}
	float GetRadHalfVFOV(UCameraComponent* Camera, float VieportAspectRatio)
	{
		return FMath::Atan(FMath::Tan(FMath::DegreesToRadians(0.5f * Camera->FieldOfView)) * 1 / VieportAspectRatio);
	}
	bool IsEven(int32 Number)
	{
		return !static_cast<bool>(Number % 2);
	}
}

ASG_SnakePawnBase::ASG_SnakePawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	// Creating CameraComponent and setting it to RootComponent with directly TopDownView
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	check(CameraComponent);
	RootComponent = CameraComponent;
}

void ASG_SnakePawnBase::BeginPlay()
{
	Super::BeginPlay();

	CreateSnakeActor();
}

void ASG_SnakePawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASG_SnakePawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveVertical", this, &ASG_SnakePawnBase::OnMoveVertical);
	PlayerInputComponent->BindAxis("MoveHorizontal", this, &ASG_SnakePawnBase::OnMoveHorizontal);
}

void ASG_SnakePawnBase::OnMoveVertical(float InputAxisValue)
{
	if (!IsValid(SnakeActor))
		return;

	if (InputAxisValue == 1.0f && SnakeActor->LastMovementDirection != EMovementDirection::Down)
		SnakeActor->LastMovementDirection = EMovementDirection::Up;
	else if (InputAxisValue == -1.0f && SnakeActor->LastMovementDirection != EMovementDirection::Up)
		SnakeActor->LastMovementDirection = EMovementDirection::Down;
}

void ASG_SnakePawnBase::OnMoveHorizontal(float InputAxisValue)
{
	if (!IsValid(SnakeActor))
		return;

	if (InputAxisValue == 1.0f && SnakeActor->LastMovementDirection != EMovementDirection::Left)
		SnakeActor->LastMovementDirection = EMovementDirection::Right;
	else if (InputAxisValue == -1.0f && SnakeActor->LastMovementDirection != EMovementDirection::Right)
		SnakeActor->LastMovementDirection = EMovementDirection::Left;
}

void ASG_SnakePawnBase::UpdatePawnLocationByGrid(const FIntPoint Dimensions, const int32 CellSize, const FTransform Transform)
{
	GridDimensions = Dimensions;
	GridCellSize = CellSize;
	GridTransform = Transform;

	check(GEngine);
	check(GEngine->GameViewport);
	check(GEngine->GameViewport->Viewport);

	auto* Viewport = GEngine->GameViewport->Viewport;
	Viewport->ViewportResizedEvent.Remove(ViewportResizedHandle);
	ViewportResizedHandle = Viewport->ViewportResizedEvent.AddUObject(this, &ASG_SnakePawnBase::OnViewportResized);

#if WITH_EDITOR
	OnViewportResized(Viewport, 0);
#endif
}

void ASG_SnakePawnBase::OnViewportResized(FViewport* Viewport, uint32 Value)
{
	if (!Viewport || Viewport->GetSizeXY().Y == 0 || GridDimensions.X == 0)
	{
		return;
	}

	const float WorldWidth = GridDimensions.Y * GridCellSize;
	const float WorldHeight = GridDimensions.X * GridCellSize;

	const float ViewportAspectRatio = static_cast<float>(Viewport->GetSizeXY().X) / Viewport->GetSizeXY().Y;
	const float GridAspectRatio = static_cast<float>(GridDimensions.Y) / GridDimensions.Y;

	check(ViewportAspectRatio);

	float LocationZ = 0.0f;

	if (ViewportAspectRatio <= GridAspectRatio)
	{
		LocationZ = 0.5f * WorldWidth / FMath::Tan(GetRadHalfHFOV(CameraComponent));
	}
	else
	{
		LocationZ = 0.5f * WorldHeight / FMath::Tan(GetRadHalfVFOV(CameraComponent, ViewportAspectRatio));
	}

	LocationZ = LocationZ * (1 + CameraOffsetByZ);

	const FVector NewPawnLocation = GridTransform.GetLocation() + FVector(0.0f, 0.0f, LocationZ);
	SetActorLocation(NewPawnLocation);
}

void ASG_SnakePawnBase::CreateSnakeActor()
{
	check(GetWorld());

	FVector SnakeSpawnLocation{ FVector(0.0f, 0.0f, GridCellSize / 2) };

	if (IsEven(GridDimensions.X) && IsEven(GridDimensions.Y))
		SnakeSpawnLocation = FVector(0.0f - GridCellSize / 2, 0.0f - GridCellSize / 2, GridCellSize / 2);
	else if (IsEven(GridDimensions.X))
		SnakeSpawnLocation = FVector(0.0f - GridCellSize / 2, 0.0f, GridCellSize / 2);
	else if (IsEven(GridDimensions.Y))
		SnakeSpawnLocation = FVector(0.0f, 0.0f - GridCellSize / 2, GridCellSize / 2);

	SnakeActor = GetWorld()->SpawnActor<ASG_SnakeActorBase>(SnakeActorClass, FTransform(SnakeSpawnLocation));
}
