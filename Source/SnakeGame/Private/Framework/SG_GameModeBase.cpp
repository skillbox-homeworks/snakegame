// Copyright © 2023 FaNtic, All rights reserved

#include "Framework/SG_GameModeBase.h"
#include "Framework/SG_SnakePawnBase.h"
#include "World/SG_GridActorBase.h"
#include "EngineUtils.h"

void ASG_GameModeBase::BeginPlay()
{
	check(GetWorld());

	// Init Grid
	GridActor = GetWorld()->SpawnActor<ASG_GridActorBase>(GridActorClass, FTransform::Identity);
	check(GridActor);

	// Init Pawn
	auto* PlayerController = GetWorld()->GetFirstPlayerController();
	check(PlayerController);

	auto* Pawn = Cast<ASG_SnakePawnBase>(PlayerController->GetPawn());
	check(Pawn);
	Pawn->UpdatePawnLocationByGrid(GridActor->Dimensions, GridActor->CellSize, FTransform::Identity);
}